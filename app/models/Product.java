package models;

import com.avaje.ebean.Page;
import play.data.format.Formats;
import play.data.validation.Constraints;
import play.db.ebean.Model;
import play.mvc.PathBindable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import java.util.Date;
import java.util.List;


@Entity
public class Product extends Model implements PathBindable<Product> {

    public static Finder<Long,Product> find =
            new Finder<Long, Product>(Long.class, Product.class);
    @Id
    public Long id;

    @Constraints.Required
    public String ean;

    @Constraints.Required
    public String name;

    @Constraints.Required
    public  Double price;

    public String description;
    @OneToMany(mappedBy = "product")

    @Formats.DateTime(pattern = "yyyy-MM-dd")
    public Date date;

    public List<StockItem> stockItems;

    public Image Picture;


    //public List<Tag> tags; // truong quan he noi voi Tag

    @ManyToMany
    public List<Tag> tags;



    public Product() {}
    public Product(String ean, String name, String description, Image picture){
        this.ean = ean;
        this.name = name;
        this.description = description;
        this.Picture = picture;
    }

    public  String toString(){
        return  String.format("%s - %s",ean,name);
    }

//    public static List<Product> findAll(){
//        return find.all();
//    }

    public static Product findByEan(String ean){
        return find.where().eq("ean",ean).findUnique();

    }



   /* public static boolean remove(Product product){
        return products.remove(product);
    }

    public void save(){
        products.remove(findByEan(this.ean));
        products.add(this);
    }*/

    @Override
    public Product bind(String key, String value) {
        return findByEan(value);
    }

    @Override
    public String unbind(String key) {
        return ean;
    }

    @Override
    public String javascriptUnbind() {
        return ean;
    }

    public static Page<Product> find(int page){// tra ve trang thay vi list
        return find.where()
                .orderBy("id asc")//sap xep tang dan theo id
                .findPagingList(10)//quy dinh kich thuoc cua trang
                .setFetchAhead(false)// co can lay tat ca du lieu mot the?
                .getPage(page);// lay trang hien tai, bat dau tu trang 0

    }
}
