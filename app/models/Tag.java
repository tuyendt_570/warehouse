package models;

import play.data.validation.Constraints;
import play.db.ebean.Model;
import play.mvc.PathBindable;

import javax.persistence.*;
import java.util.*;

/**
 * Created by DoTuyen on 3/21/2015.
 */

@Entity
public class Tag extends Model implements PathBindable<Tag> {
    public static Finder<Long, Tag> find = new Finder<>(Long.class, Tag.class);

    @Id
    public Long id;

    @Constraints.Required
    public String name;

    @ManyToMany(mappedBy = "tags")
    public List<Product> products;

  //  private static List<Tag> tags = new LinkedList<Tag>();


    public static Tag findById(Long id) {
        return find.byId(id);
    }

    public static List<Tag> findAll() {
        return find.all();
    }

    public Tag(){
        //left empty
    }
    public Tag(Long id, String name, Collection<Product> products){
        this.id = id;
        this.name = name;
        this.products = new LinkedList<Product>(products);
        for(Product product: products){
            product.tags.add(this);
        }
    }

    @Override
    public Tag bind(String s, String value) {
        return findByName(value);
    }

    private Tag findByName(String value) {
        return find.where().eq("name",value).findUnique();
    }

    @Override
    public String unbind(String key) {
        return key;
    }

    @Override
    public String javascriptUnbind() {
        return name;
    }
}
