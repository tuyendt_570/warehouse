package models;

import play.db.ebean.Model;
import javax.persistence.*;

/**
 * Created by DoTuyen on 3/27/2015.
 */

@Entity
public class StockItem extends Model {

    public static Model.Finder<Long,StockItem> find =
            new Model.Finder<Long,StockItem>(Long.class, StockItem.class);

    @Id
    public Long id;

    @ManyToOne
    public Warehouse warehouse;

    @ManyToOne
    public Product product;

    public Long quantity;

    public String toString(){
        return String.format("StockItem %d - %d x product %s",
                id, quantity, product == null ? null : product.id);
    }
}
