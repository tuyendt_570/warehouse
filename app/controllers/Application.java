package controllers;


import models.Image;
import models.UserAccount;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import views.html.index;
import views.html.login;

import static play.data.Form.form;

public class Application extends Controller {

    public static Result index() {// duoc goi khi vao trang chu, do duoc dinh nghia trong conf/routes
        //cu the la dong:      GET  /  controllers.Application.index()
        return ok(index.render("Hello............................................."));//index.render() co nghia la goi den templete index.scala.html
    }

    public static Result uploadImagine() {
        Form<UploadImageForm> form = form(UploadImageForm.class).bindFromRequest();

        if (form.hasErrors()) {
            return badRequest();
        } else {
            new Image(
                    form.get().image.getFilename(),
                    form.get().image.getFile()
            );

            flash("success", "File uploaded.");
            return redirect(routes.Application.index());
        }

    }

    public static Result getImagine(long id) {
        Image image = Image.find.byId(id);

        if (image != null) {

            /*** here happens the magic ***/
            return ok(image.data).as("image");
            /************************** ***/

        } else {
            flash("error", "Picture not found.");
            return redirect(routes.Application.index());
        }
    }

    public static class Login {
        public String email;
        public String password;
    }

    public static Result login() {
        return ok(
                login.render(form(Login.class))
        );
    }

    public static Result logout() {
        session().clear();
        return redirect(routes.Application.index());
    }

    public static Result authenticate() {
        Form<Login> loginForm = form(Login.class).bindFromRequest();
        String email = loginForm.get().email;
        String password = loginForm.get().password;

        session().clear();
        if (UserAccount.authenticate(email, password) == null) {
            flash("error", "Invalid email and/or password");
            return redirect(routes.Application.login());
        }
        session("email", email);

        return redirect(routes.Products.list(0));
    }

    public static class UploadImageForm {
        public Http.MultipartFormData.FilePart image;

        public String validate() {
            Http.MultipartFormData data = request().body().asMultipartFormData();
            image = data.getFile("image");

            if (image == null) {
                return "File is missing.";
            }

            return null;
        }
    }
}
