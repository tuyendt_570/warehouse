package controllers;

import models.Tag;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.Tag_detail;
import views.html.listTag;

import java.util.List;

/**
 * Created by DoTuyen on 4/11/2015.
 */
    public class Tags extends Controller {
        public static Result listTags(){
            List<Tag> tags = Tag.findAll();
            return ok(listTag.render(tags));
        }

        public static Result detail(Tag tag){
            return ok(Tag_detail.render(tag.products));
        }
    }


