package controllers;

import models.StockItem;
import play.mvc.Controller;
import play.mvc.Result;

import java.util.List;

/**
 * Created by DoTuyen on 3/29/2015.
 */
public class StockItems extends Controller{


    public static Result index(){
        List<StockItem> items = StockItem.find
                .where()
                .ge("quantity", 300)
                .orderBy("quantity")
                //.setMaxRows(5)
                .findList();
        return  ok(items.toString());

    }
}
