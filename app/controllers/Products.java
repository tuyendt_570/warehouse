package controllers;

import com.avaje.ebean.Page;
import models.Product;
import models.StockItem;
import models.Tag;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import views.html.products.details;
import views.html.products.product_details;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by DoTuyen on 3/14/2015.
 */

@Security.Authenticated(Secured.class)
public class Products extends Controller {
    private static final Form<Product> productForm = Form.form(Product.class);
    public static Result list(Integer page){
        Page<Product>products = Product.find(page);
        //return ok(list.render(products));
        return ok(views.html.catalog.render(products));
    }

    public static  Result newProduct(){
        return ok(views.html.products.details.render(productForm));
    }

    public static Result details(Product product) {
        if(product ==null){
            return notFound(String.format("Product % does not exist.",product.ean));
        }
        return ok(product_details.render(product));
    }
    // luu tru cac doi tuong tu webform
    public static Result save(){
        Form<Product> boundForm = productForm.bindFromRequest();
        if (boundForm.hasErrors()) {
            flash("error", "Please correct the form below.");
            return badRequest(views.html.products.details.render(boundForm));
        }
        // binding and error handling
        Product product = boundForm.get();

        //Ebean.save(product);// save product nhung chua ktra id
      //  return redirect(routes.Products.list());
        List<Tag> tags = new ArrayList<Tag>();
        for(Tag tag: product.tags){
            if(tag.id != null){
                tags.add(Tag.findById(tag.id));
            }
        }
        product.tags =tags;

        StockItem stockItem = new StockItem();
        stockItem.product = product;
        stockItem.quantity = 0L;

        if(product.id == null){
            if(Product.findByEan(product.ean)== null) {
                product.save();
                flash("success", String.format("Successfully added product %s", product));
            }else {
                flash("success", String.format("product has exist %s", product));
            }
        }else {
            product.update();
            flash("success", String.format("Successfully updated product %s", product));
        }


      //  product.save();
        stockItem.save();

        return redirect(routes.Products.list(0));
    }

    public static Result delete(String ean){
        final Product product = Product.findByEan(ean);
        if(product ==null){
            return notFound(String.format("Product % does not exists.",ean));
        }

        //xoa cac stockItem co lien quan den product trc khi xoa product
        for(StockItem stockItem : product.stockItems){
            stockItem.delete();
        }
        for(Tag tag : product.tags){
            tag.products.remove(product);
            tag.update();
        }
        product.delete();
        return redirect(routes.Products.list(0));
    }

    public static Result edit(Product product){
        if (product == null){
            return notFound(String.format("Product %s not exist", product.ean));
        }
        Form<Product> filledForm = productForm.fill(product);
        return ok(details.render(filledForm));
    }
}
