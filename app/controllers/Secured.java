package controllers;

import play.mvc.Http.Context;
import play.mvc.Security;
import play.mvc.Result;
/**
 * Created by DoTuyen on 4/4/2015.
 */
public class Secured extends Security.Authenticator {
    @Override
    public String getUsername(Context ctx){
        return ctx.session().get("email");
    }
    @Override
    public Result onUnauthorized(Context ctx) {
        return redirect(routes.Application.login());
    }
}
